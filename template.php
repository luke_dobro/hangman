<?php
class Template
{
    public $template;

    public function __construct()
    {
        $this->template = file_get_contents('index.html', true);
    }
    public function save_template($vars)
    {
        replace_tags($this->template, $vars);
    }
    public function prepare_parameters($slovo, $hlaska, $zivoty)
    {
        $vars = array('{{SLOVO}}' => $slovo, '{{HLASKA}}' => $hlaska, '{{ZIVOTY}}' => $zivoty,);
        return $vars;
    }
    public function replace_tags($params)
    {
        return str_replace(array_keys($params), $params, $this->template);
    }
}