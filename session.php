<?php
require_once 'slovo.php';
class Session
{
    public function __construct()
    {

    }
    public function zobraz_slovo()
    {
        return $_SESSION['hadane_slovo'];
    }

    public function uloz_slovo($slovo)
    {
        $_SESSION['hadane_slovo']=$slovo;
    }
    public function uloz_zatim_uhodnuto($zatim_uhodnuto)
    {
        $_SESSION['zatim_uhodnuto']=$zatim_uhodnuto;
    }
    public function get_zatim_uhodnuto()
    {
        return $_SESSION['zatim_uhodnuto'];
    }
    public function uloz_zivoty($pocet_zivotu)
    {
        $_SESSION['zivoty']=$pocet_zivotu;
    }
    public function get_zivoty()
    {
        return $_SESSION['zivoty'];
    }
}