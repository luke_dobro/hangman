<?php
class Hrac
{
    private $pocet_zivotu;

    public function __construct()
    {
        $this->pocet_zivotu = 10;
    }
    public function neuhodl_pismeno()
    {
        $this->pocet_zivotu--;
        return $this->pocet_zivotu;
    }
    public function get_pocet_zivotu()
    {
        return $this->pocet_zivotu;
    }
    public function set_pocet_zivotu($zivoty)
    {
        return $this->pocet_zivotu=$zivoty;
    }
    public function neuhodl_slovo()
    {
        return $this->pocet_zivotu=0;
    }
}