<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

require_once 'slovo.php';
require_once 'hrac.php';
require_once 'session.php';
require_once 'template.php';
require_once 'form.php';

$slovo = new slovo();
$session = new session();
$template = new template();
$form = new form();
$hrac= new hrac();

if ($form->stisknute_tlacitko()=='hadat')
{
    if ($form->pismeno_nebo_slovo()=='pismeno')
    {
        $slovo->vygenerovane_slovo=$_SESSION['hadane_slovo'];
        $slovo->vypomlckovane_slovo=$_SESSION['zatim_uhodnuto'];
        $hrac->set_pocet_zivotu($_SESSION['zivoty']);
        if ($slovo->nachazi_se_pismeno_ve_slove($form->get_pismeno())!== false)
        {
            $session->uloz_zatim_uhodnuto($slovo->zatim_uhodnuto($form->get_pismeno()));
            echo $template->replace_tags($template->prepare_parameters($session->get_zatim_uhodnuto(),'Správně! Písmeno '. $form->get_pismeno().' se v hledaném slově nachází.',$session->get_zivoty()));
        }
        else
        {
            $hrac->neuhodl_pismeno();
            $session->uloz_zivoty($hrac->get_pocet_zivotu());
            if ($hrac->get_pocet_zivotu() == 0)
            {
                echo $template->replace_tags($template->prepare_parameters($slovo->vypomlckovane_slovo,'A jsi po smrti.',0));
                echo "<script>document.getElementById('hadat').style.cssText = 'display: none';</script>";
            }
            else echo $template->replace_tags($template->prepare_parameters($session->get_zatim_uhodnuto(),'Špatně! Písmeno '. $form->get_pismeno(). ' se v hledaném slově nenachází.',$session->get_zivoty()));
        }
    }
    else if ($form->pismeno_nebo_slovo()=='slovo')
    {
        $slovo->vygenerovane_slovo=$_SESSION['hadane_slovo'];
        $slovo->vypomlckovane_slovo=$_SESSION['zatim_uhodnuto'];
        $hrac->set_pocet_zivotu($_SESSION['zivoty']);

        if ($slovo->if_uhodl_slovo($form->get_slovo())== true)
        {
            $session->uloz_zatim_uhodnuto($_SESSION['hadane_slovo']);
           echo $template->replace_tags($template->prepare_parameters($session->get_zatim_uhodnuto(),'Gratulace, uhodl jsi celé slovo!',$session->get_zivoty()));
        }
        else
        {
            echo $template->replace_tags($template->prepare_parameters($slovo->vypomlckovane_slovo,'A jsi po smrti.',0));
            echo "<script>document.getElementById('hadat').style.cssText = 'display: none';</script>";
        }
    }
}
else if ($form->stisknute_tlacitko()=='start')
{
    $slovo->vygenerovane_slovo();
    $session->uloz_zatim_uhodnuto($slovo->vypomlckovane_slovo);
    $session->uloz_slovo($slovo->vygenerovane_slovo);
    $session->uloz_zivoty($hrac->get_pocet_zivotu());
    echo $template->replace_tags($template->prepare_parameters($slovo->vypomlckovane_slovo,'Hádejte první písmeno.',10));
}
else
{
    echo $template->replace_tags($template->prepare_parameters('','Stisknutím tlačítka Start začněte hru.',''));
}