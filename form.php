<?php
class Form
{
    public $hadany_retezec;

    public function stisknute_tlacitko()
    {
        if (isset($_POST['hadat']))
            return "hadat";
        else if (isset($_POST['start']))
        {
            return "start";
        }
        else
        {
            session_destroy();
            return "konec";
        }
    }
    public function pismeno_nebo_slovo()
    {
        if (($_POST['radio'] == 'pismeno'))
            return "pismeno";
        else
            return "slovo";
    }
    public function get_pismeno()
    {
        $this->hadany_retezec=$_POST['hadane-pismeno'];
        return $this->hadany_retezec;
    }
    public function get_slovo()
    {
        $this->hadany_retezec=$_POST['hadane-slovo'];
        return $this->hadany_retezec;
    }
}